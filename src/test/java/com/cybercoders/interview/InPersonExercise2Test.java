package com.cybercoders.interview;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class InPersonExercise2Test {
    @Test
    public void testIsPrimePalindrome() throws Exception {
        assertThat(InPersonExercise2.isPrimePalindrome(7), is(true));
        assertThat(InPersonExercise2.isPalindrome(177771), is(true));
        assertThat(InPersonExercise2.isPrime(177771), is(false));
        assertThat(InPersonExercise2.isPrimePalindrome(177771), is(false));
    }
}