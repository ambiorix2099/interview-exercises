package com.cybercoders.interview;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class InPersonExercise1Test {
    @Test
    public void testCanBuildBrickRow() throws Exception {
        assertThat(InPersonExercise1.canBuildBrickRow(3, 6, 31), is(true));
        assertThat(InPersonExercise1.canBuildBrickRow(4, 7, 36), is(true));
        assertThat(InPersonExercise1.canBuildBrickRow(4, 7, 3), is(true));
    }
}