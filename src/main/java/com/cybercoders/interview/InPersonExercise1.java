package com.cybercoders.interview;

/**
 * This is a brute-force implementation of the stated requirements.
 * Immediately after implementing it, I realized the problem was simply a variation
 * on problems involving finding the factors of a given number.
 * That realization, had it come sooner, would've lead to perhaps a leaner solution.
 */
public class InPersonExercise1 {
    public static final int BIG_BRICK_SIZE = 5;
    public static final int SMALL_BRICK_SIZE = 1;

    public static boolean canBuildBrickRow(int numberOfSmallBricks, int numberOfBigBricks, int rowLengthGoal) {
        // Favor BIG bricks by successively adding them FIRST
        int currentRowLength;
        for (int bigBrickCount = 0; bigBrickCount <= numberOfBigBricks; bigBrickCount++) {
            currentRowLength = bigBrickCount * BIG_BRICK_SIZE;

            // We've reached the goal with only big bricks, we're done!
            if (currentRowLength == rowLengthGoal) {
                return true;
            }

            // No point in adding more small bricks if we're already past the row goal.
            if (currentRowLength > rowLengthGoal) {
                break;
            }

            // A quick check to see if it's worth successively adding smaller bricks.
            if (currentRowLength + numberOfSmallBricks * SMALL_BRICK_SIZE < rowLengthGoal) {
                continue;
            }

            // Now try successively adding small bricks
            int bigBricksOnlyRowLength = currentRowLength;
            for (int smallBrickCount = 0; smallBrickCount <= numberOfSmallBricks; smallBrickCount++) {
                currentRowLength = bigBricksOnlyRowLength + smallBrickCount * SMALL_BRICK_SIZE;
                if (currentRowLength == rowLengthGoal) {
                    return true;
                } else if (currentRowLength > rowLengthGoal) {
                    break;
                }
            }
        }

        return false;
    }
}