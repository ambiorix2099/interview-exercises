package com.cybercoders.interview;

import java.util.ArrayList;
import java.util.List;

public class InPersonExercise2 {
    public static boolean isPrimePalindrome(int input) {
        return isPalindrome(input) && isPrime(input);
    }

    public static boolean isPalindrome(int input) {
        input = normalizeInput(input);

        if (input < 10) {
            return true;
        }

        List<Integer> palindromeCandidate = new ArrayList<Integer>();

        int mutableInput = input;
        int currentDecimalPlace = 10;
        while (mutableInput * 10 >= currentDecimalPlace) {
            int modulo = mutableInput % (currentDecimalPlace);
            palindromeCandidate.add(modulo);
            mutableInput = (mutableInput - modulo);
            currentDecimalPlace *= 10;
        }

        int k = 0;
        int powerOf10 = palindromeCandidate.size() - 1;
        for (int i = 0; i < palindromeCandidate.size(); i++) {
            k += (palindromeCandidate.get(i) / (int) Math.pow(10, i)) * (int) Math.pow(10, powerOf10);
            powerOf10--;
        }

        return k == input;
    }

    public static boolean isPrime(int input) {
        input = normalizeInput(input);

        if (input != 2 && input % 2 == 0) {
            return false;
        }

        int factorCount = 0;
        for (int i = input; i > 1; i--) {
            if (input % i == 0) {
                factorCount++;
                if (factorCount > 1) {
                    return false;
                }
            }

        }

        return true;
    }

    /**
     * Normalizes the input so, for example, we can handle negative integers.
     */
    private static int normalizeInput(int input) {
        return Math.abs(input);
    }
}